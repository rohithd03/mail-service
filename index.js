const express = require("express");
const nodemailer = require("nodemailer");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.post("/", function(req, res){

    const transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'logan.toy95@ethereal.email',
            pass: 'JbDhhPgr3QXDJxVj1b'
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    let message = {
        from: 'sender@example.com',
        to: req.body.to,
        subject: 'Mail from Nodemailer',
        text: req.body.email_body
    };

    transporter.sendMail(message, (err, info) => {
        if (err) {
            console.log('Error occurred. ' + err);
            return res.send({"success": "false", "message": err});
        }
        
        return res.send({"success": "true", "message": "Email sent successfully"});
    });
});

app.listen(3000, function(){
  console.log("Server started at port 3000");
});
